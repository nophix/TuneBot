import datetime
import time
from typing import Optional

import discord
import humanize
import lavalink
from discord.ext import commands
from discord.ext.commands import Context

from bot import TuneBot
from context import CustomContext
from utils.classes import BaseCog
from utils.EmbedGenerator import EmbedGenerator
from utils.paginator import HelpPaginator


class InformationCog(BaseCog, name="Information"):
    @commands.command(name="ping", aliases=["pong"])
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    async def ping(self, ctx: Context):
        """Test the latency"""
        avatar = ctx.author.avatar.with_static_format("jpeg")
        emoji = discord.utils.get(ctx.bot.emojis, name="loading")
        start = time.monotonic()
        msg = await ctx.send(
            embed=discord.Embed(description=f"{emoji} Calculating ping")
        )
        millis = (time.monotonic() - start) * 1000
        heartbeat = ctx.bot.latency * 1000
        embed = discord.Embed(color=discord.Color.blue())
        embed.add_field(
            name=":heartbeat: Heartbeat", value=f"`{heartbeat:,.2f}ms`", inline=True
        )
        embed.add_field(
            name=":file_cabinet: ACK", value=f"`{millis:,.2f}ms`", inline=True
        )
        embed.set_footer(text=f"Requested by: {ctx.author}", icon_url=f"{avatar}")
        await msg.edit(embed=embed)

    @commands.command(name="invite")
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    async def invite(self, ctx: Context):
        """Gets the invite link"""
        await EmbedGenerator.Message(
            ctx, "Add our bot to your server:", self.bot.invite_link
        )

    @commands.command(name="wlinfo")
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    async def wlinfo(self, ctx: CustomContext):
        """Retrieve various node/server/player information"""
        player = self.bot.lavalink.player_manager.get(ctx.guild.id)
        nodes = self.bot.lavalink.node_manager.available_nodes

        used = humanize.naturalsize(sum([n.stats.memory_used for n in nodes]))
        total = humanize.naturalsize(sum([n.stats.memory_allocated for n in nodes]))
        free = humanize.naturalsize(sum([n.stats.memory_free for n in nodes]))
        cpu = sum([n.stats.cpu_cores for n in nodes])

        fmt = (
            f"**Lavalink:** `{lavalink.__version__}`\n\n"
            f"Connected to `{len(self.bot.lavalink.node_manager.available_nodes)}` nodes.\n"
            # f"Best available Node `{self.bot.lavalink.node_manager.find_ideal_node().name.__repr__()}`\n"
            f"`{len(self.bot.lavalink.player_manager.players)}` players are distributed on nodes.\n"
            f"`{sum([n.stats.players for n in nodes])}` players are distributed on server.\n"
            f"`{sum([n.stats.playing_players for n in nodes])}` players are playing on server.\n\n"
            f"Server Memory: `{used}/{total}` | `({free} free)`\n"
            f"Server CPU: `{cpu}`\n\n"
            # f"Server Uptime: `{datetime.timedelta(milliseconds=node.stats.uptime)}`"
        )
        await ctx.send(fmt)

    @commands.command(name="help", aliases=["about", "info"])
    @commands.cooldown(1, 1, commands.BucketType.user)
    async def about(
        self,
        ctx: Context,
        command: Optional[str] = commands.Option(
            description="Show help for a command or category"
        ),
    ):
        """Retrieve a list of possible commands"""
        if command:
            entity = self.bot.get_cog(command) or self.bot.get_command(command)

            if entity is None:
                clean = command.replace("@", "@\u200b")
                return await ctx.send(f'Command or category "{clean}" not found.')
            elif isinstance(entity, discord.ext.commands.Command):
                p = await HelpPaginator.from_command(ctx, entity)
            else:
                p = await HelpPaginator.from_cog(ctx, entity)
            return await p.paginate()

        info = self.bot.config["info"]
        title = info["name"] + " Help"
        descr = info["description"]
        color = self.bot.colors["embed"]

        embed = discord.Embed(color=color, title=title, description=descr)

        display_cogs = {
            "Information": ":information_source:",
            "Music": ":musical_note:",
            "Settings": ":gear:",
        }

        for cog_name, cog_icon in display_cogs.items():
            cog = self.bot.cogs.get(cog_name)
            if not cog:
                continue
            cogname_str = f"{cog_icon} {cog_name}"
            commands = [f"`{cmd.name}`" for cmd in cog.get_commands() if not cmd.hidden]
            commands_str = ", ".join(commands)
            embed.add_field(name=cogname_str, value=commands_str, inline=False)

        avatar = ctx.author.avatar.with_static_format("jpeg")
        embed.set_footer(text=f"Requested by: {ctx.author}", icon_url=avatar)
        await ctx.send(embed=embed)


def setup(bot: TuneBot):
    bot.remove_command("help")
    bot.add_cog(InformationCog(bot))
