from typing import TYPE_CHECKING

from aioredis.client import Redis
from discord import Embed
from discord.ext import commands
from discord.ext.commands.errors import CommandInvokeError
from lavalink.models import DefaultPlayer

from tunebot.redis import RedisAutoJoin
from tunebot.redis import RedisPlaylistSource

if TYPE_CHECKING:
    from bot import TuneBot
    from tunebot import PlaylistSource
    from tunebot import AutoJoin
    from utils.classes import BaseCog


class CustomContext(commands.Context):
    bot: "TuneBot"
    cog: "BaseCog"

    @property
    def redis(self) -> Redis:
        return self.bot._redis_client

    @property
    def player(self) -> DefaultPlayer:
        if self.cog.is_lavalink_ready():
            return self.bot.lavalink.player_manager.get(self.guild.id)

        raise CommandInvokeError("Lavalink is still starting up.")

    def create_embed(self) -> Embed:
        bot: TuneBot = self.bot
        color = bot.colors["embed"]

        avatar = None
        if avatar_asset := self.author.avatar:
            avatar = avatar_asset.with_static_format("jpeg")

        embed = Embed(color=color)
        embed.set_footer(text=f"Requested by: {self.author}", icon_url=avatar)
        return embed

    @property
    def playlist_source(self) -> "PlaylistSource":
        if not hasattr(self, "_playlist_source"):
            self._playlist_source = RedisPlaylistSource(self)
        return self._playlist_source

    @property
    def autojoin(self) -> "AutoJoin":
        if not hasattr(self, "_autojoin"):
            self._autojoin = RedisAutoJoin(self)
        return self._autojoin
