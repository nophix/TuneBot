import json
import subprocess
import sys

import redis
from yt_dlp import YoutubeDL

if len(sys.argv) < 2:
    raise Exception("Expected youtube playlist/channel/video")

config_path = sys.argv[1]

config = json.load(open("config.json", "r", encoding="utf-8"))
redis_prefix = config["redis_prefix"]
redis_client = redis.from_url(config["redis_url"])

file = open(config_path, "r")

youtube_dl_opts = {}

vid_urls = subprocess.run(["yt-dlp", "--print", "id", f"{sys.argv[1]}"], capture_output=True).stdout.decode("utf-8")

for vid_url in vid_urls.split("\n"):
    if vid_url == "":
        continue
    vid_url = "https://www.youtube.com/watch?v=" + vid_url
    redis_client.sadd(f"{redis_prefix}:playlist", vid_url)
    print(vid_url)
