import json
import sys

import redis
from aiotube import Playlist

if len(sys.argv) < 2:
    raise Exception("Expected path to file as argument")

config_path = sys.argv[1]

config = json.load(open("config.json", "r", encoding="utf-8"))
redis_prefix = config["redis_prefix"]
redis_client = redis.from_url(config["redis_url"])

file = open(config_path, "r")
for line in file:
    playlist = Playlist(line)
    for vid in playlist.videos():
        yt_url = vid.url
        redis_client.sadd(f"{redis_prefix}:playlist", yt_url)
        print(yt_url)
