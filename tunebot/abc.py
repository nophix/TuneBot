from abc import ABC
from abc import abstractmethod
from typing import Optional


class GlobalPlaylistSource(ABC):
    @abstractmethod
    async def fetch_sources(self) -> set[str]:
        pass


class PlaylistSource(ABC):
    @abstractmethod
    async def add(self, source_url: str):
        pass

    @abstractmethod
    async def remove(self, source_url: str) -> bool:
        pass


class GlobalPlaylist(ABC):
    @abstractmethod
    async def pick_random(self, amount: Optional[int] = 1) -> set[str]:
        pass

    @abstractmethod
    async def add_tracks(self, urls: list[str]):
        pass

    @abstractmethod
    async def clear(self):
        pass


class GlobalAutoJoin(ABC):
    @abstractmethod
    async def fetch_channels(self) -> dict[str, list[str]]:
        pass


class AutoJoin(ABC):
    @abstractmethod
    async def update(self, voice_channel_id: int, text_channel_id: int):
        pass

    @abstractmethod
    async def disable(self):
        pass


__all__ = (
    "GlobalPlaylistSource",
    "PlaylistSource",
    "GlobalPlaylist",
    "GlobalAutoJoin",
    "AutoJoin",
)
