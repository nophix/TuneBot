from tunebot import AutoJoin
from tunebot import GlobalAutoJoin
from tunebot.redis import RedisBotEntity
from tunebot.redis import RedisContextEntity


class GlobalRedisAutoJoin(RedisBotEntity, GlobalAutoJoin):
    async def fetch_channels(self) -> dict[str, list[str]]:
        """
        Retrieves all guilds (with their configurations) where AutoJoin is enabled
        """
        channels: dict[str, str] = await self.redis.hgetall(self.key("autojoin"))
        return {key: value.split(":") for key, value in channels.items()}


class RedisAutoJoin(RedisContextEntity, AutoJoin):
    async def update(self, voice_channel_id: int, text_channel_id: int):
        """
        Upserts the configuration of an AutoJoin guild.

        Args:
            voice_channel_id (int): [description]
            text_channel_id (int): [description]
        """
        if not self.ctx.guild:
            raise Exception("This method can only be invoked inside of a guild.")

        value = f"{voice_channel_id}:{text_channel_id}"
        await self.redis.hset(self.key("autojoin"), self.ctx.guild.id, value)

    async def disable(self):
        """
        Removes the AutoJoin configuration of a guild.
        """
        if not self.ctx.guild:
            raise Exception("This method can only be invoked inside of a guild.")

        await self.redis.hdel(self.key("autojoin"), self.ctx.guild.id)


__all__ = ("GlobalRedisAutoJoin", "RedisAutoJoin")
