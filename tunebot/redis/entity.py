from abc import ABC
from abc import abstractmethod
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from context import CustomContext

from aioredis.client import Redis


class RedisEntity(ABC):
    @property
    @abstractmethod
    def redis() -> Redis:
        pass

    @abstractmethod
    def key(self, name: str) -> str:
        pass


class RedisBotEntity(RedisEntity):
    def __init__(self, redis: Redis, prefix: str) -> None:
        self._redis = redis
        self.prefix = prefix
        super().__init__()

    @property
    def redis(self) -> Redis:
        return self._redis

    def key(self, name: str) -> str:
        return ":".join([self.prefix, name])


class RedisContextEntity(RedisEntity):
    def __init__(self, ctx: "CustomContext") -> None:
        self.ctx = ctx
        super().__init__()

    @property
    def redis(self) -> Redis:
        return self.ctx.redis

    def key(self, name: str) -> str:
        return ":".join([self.ctx.bot.redis_prefix, name])


__all__ = ("RedisEntity", "RedisBotEntity", "RedisContextEntity")
