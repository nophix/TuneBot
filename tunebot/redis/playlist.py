from typing import Optional

from tunebot.abc import GlobalPlaylist
from tunebot.redis import RedisBotEntity


class GlobalRedisPlaylist(RedisBotEntity, GlobalPlaylist):
    async def pick_random(self, amount: Optional[int] = 1) -> set[str]:
        """
        Picks an amount of random tracks from the playlist in Redis

        Args:
            amount (Optional[int], optional): [description]. Defaults to 1.
        """
        return await self.redis.srandmember(self.key("playlist"), amount)

    async def add_tracks(self, urls: list[str]):
        """
        Adds one or more urls to the playlist in Redis

        Args:
            urls (list[str]): [description]
        """
        await self.redis.sadd(self.key("playlist"), *urls)

    async def clear(self):
        """
        Clears the entire playlist in Redis
        """
        await self.redis.delete(self.key("playlist"))


__all__ = ("GlobalRedisPlaylist",)
