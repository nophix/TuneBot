from tunebot import GlobalPlaylistSource
from tunebot import PlaylistSource
from tunebot.redis import RedisBotEntity
from tunebot.redis import RedisContextEntity


class GlobalRedisPlaylistSource(RedisBotEntity, GlobalPlaylistSource):
    async def fetch_sources(self) -> set[str]:
        """
        Fetches all sources
        """
        return await self.redis.smembers(self.key("sources"))


class RedisPlaylistSource(RedisContextEntity, PlaylistSource):
    async def add(self, source_url: str):
        """
        Adds a playlist source to Redis

        Args:
            source_url (str): [description]
        """
        await self.redis.sadd(self.key("sources"), source_url)

    async def remove(self, source_url: str) -> bool:
        """
        Removes a playlist source from Redis

        Args:
            source_url (str): [description]
        """
        return await self.redis.srem(self.key("sources"), source_url)


__all__ = ("GlobalRedisPlaylistSource", "RedisPlaylistSource")
