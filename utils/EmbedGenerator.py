from typing import Optional
from typing import Union

import discord
from discord import Embed
from discord.ext.commands import Context


class EmbedGenerator:
    @staticmethod
    async def Error(ctx: Context, message: str, **kwargs) -> Embed:
        color = ctx.bot.colors["embed"]
        em = Embed(title="Error:", description=message, color=color)
        return await EmbedGenerator.SendWithFooter(ctx, em, **kwargs)

    @staticmethod
    async def Message(
        ctx: Context, title: str, message: Optional[str] = "", **kwargs
    ) -> Embed:
        color = ctx.bot.colors["embed"]
        em = Embed(title=title, description=message, color=color)
        return await EmbedGenerator.SendWithFooter(ctx, em, **kwargs)

    @staticmethod
    async def Image(
        ctx: Context, title: str, url: str, message: Optional[str] = "", **kwargs
    ) -> Embed:
        color = ctx.bot.colors["embed"]
        em = Embed(title=title, description=message, url=url, color=color)
        em.set_image(url=url)
        return await EmbedGenerator.SendWithFooter(ctx, em, **kwargs)

    @staticmethod
    async def Title(ctx: Context, title: str, **kwargs) -> Embed:
        color = ctx.bot.colors["embed"]
        em = Embed(title=title, color=color)
        return await EmbedGenerator.SendWithFooter(ctx, em, **kwargs)

    @staticmethod
    async def SendWithFooter(
        ctx: Context, em: Embed, **kwargs
    ) -> Union[discord.Message, Embed]:
        avatar = ctx.author.avatar.with_static_format("jpeg")
        em.set_footer(text=f"Requested by: {ctx.author}", icon_url=avatar)
        if kwargs.get("no_send", False):
            return em
        return await ctx.send(embed=em, **kwargs)
