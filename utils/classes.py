import asyncio
from typing import Dict

from discord.ext.commands import Cog

from bot import TuneBot


class BaseCog(Cog):
    def __init__(self, bot: TuneBot) -> None:
        self.bot = bot

        slash_descriptions: Dict[str, str] = self.bot.config["slash_descriptions"]
        for command in self.walk_commands():
            if brief := slash_descriptions.get(command.qualified_name):
                command.brief = brief

    def is_lavalink_ready(self) -> bool:
        return (
            hasattr(self.bot, "lavalink")
            and len(self.bot.lavalink.node_manager.available_nodes) > 0
        )
