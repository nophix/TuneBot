from typing import Callable
from typing import TYPE_CHECKING
from typing import TypeVar

from discord.ext.commands import check
from discord.ext.commands.errors import NotOwner

if TYPE_CHECKING:
    from context import CustomContext

T = TypeVar("T")


def source_manager_only() -> Callable[[T], T]:
    """
    A :func:`.check` that checks if the person invoking this command is allowed to modify the radio sources.
    """

    async def predicate(ctx: "CustomContext") -> bool:
        is_manager = ctx.author.id in ctx.bot.config["manager_ids"]
        is_owner = await ctx.bot.is_owner(ctx.author)
        if not is_manager and not is_owner:
            raise NotOwner("You are not allowed to modify the sources.")

        return True

    return check(predicate)
