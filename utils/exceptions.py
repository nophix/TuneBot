from discord.embeds import Embed
from discord.ext.commands import CommandError

from context import CustomContext


class EmbeddedCommandException(CommandError):
    def __init__(self, embed: Embed) -> None:
        self.embed = embed

    async def send(self, ctx: CustomContext):
        await ctx.send(embed=self.embed)
